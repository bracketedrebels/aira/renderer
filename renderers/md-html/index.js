const remarkable = require('remarkable')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')

const read = promisify(fs.readFile)
const put = promisify(fs.writeFile)

module.exports = async function(source, sandbox) {
  const contents = (await read(source)).toString()
  const renderer = new remarkable('full')
  const rendered = renderer.render(contents)
  const outFile = path.resolve(sandbox, 'output.html')
  await put(outFile, rendered)
  return outFile
}
