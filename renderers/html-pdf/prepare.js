const zip = require('adm-zip')
const exists = require('fs').exists
const path = require('path')
const promisify = require('util').promisify
const validator = new require('jsonschema').Validator()
const entrypoint = 'manifest.json'
const schema = require('fs').readFileSync(path.resolve(__dirname, 'template.schema.json')).toString()

module.exports = async (path, sandbox) => {
  const real = await exists(path)
  if (!real)
    throw new Error('Provided template archive does not exist')
  
  const archive = new zip(path)
  const entries = archive.getEntries()
  if (!entries.some(v => v.entryName === entrypoint))
    throw new Error('Template archive does not contain `manifest.json` file')
  
  const read = promisify(archive.readAsTextAsync.bind(archive))
  const manifest = await read(entrypoint)
  const validation = validator.validate(manifest, schema)
  if (!validation.valid)
    throw new Error(`Template manifest is malformed: ${validation.errors[0]}`)
  
  const extract = promisify(archive.extractAllToAsync.bind(archive))
  const target = path.resolve(sandbox, 'template') 
  await extract(target, true)
  return target
}