const wkhtml2pdf = require('wkhtmltopdf')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')

const read = promisify(fs.readFile)
const put = promisify(fs.writeFile)
const prepare = require('./prepare')

module.exports = async function(source, sandbox, payload) {
  const unpacked = await prepare(payload)
}
