const exists = require('fs').existsSync

module.exports = renderers => args => {
  const sourceExists = exists(args.source)
  if (!sourceExists)
    throw new Error('Source data file does not exist')
  
  const ins = [args.format, ...args.formats.slice(0, -1)]
  const outs = args.formats.slice()

  const unresolved = ins
    .map((v, i) => [v, outs[i]])
    .find(v => renderers.every(({manifest: {input, output}}) => input !== v[0] || output !== v[1]))
  if (unresolved)
    throw new Error(`Unable to detect renderer for ${unresolved[0]} to ${unresolved[1]} pipe`)
  
  const templateMissed = ins
    .map((v, i) => [v, outs[i]])
    .find(v => renderers
      .find(({manifest: {input, output}}) => input === v[0] && output === v[1] )
      .manifest.templateRequired === true && !args[v[1]])
  if (templateMissed)
    throw new Error(`No required template provided for renderer from ${templateMissed[0]} to ${templateMissed[1]}`)

  return true
}