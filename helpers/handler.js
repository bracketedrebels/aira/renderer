const path = require('path')
const process = require('process')
const {promisify} = require('util')
const rmdir = promisify(require('rimraf'))
const mkdir = promisify(require('mkdirp'))
const cpfile = promisify(require('fs').copyFile)

module.exports = renderers => async args => {
  const destination = args.out
    ? (path.isAbsolute(args.out)
      ? args.out
      : path.resolve(process.cwd(), args.out))
    : path.resolve(process.cwd(), `output.${args.formats[args.formats.length - 1]}`);
  const localFldr = path.resolve(path.dirname(destination), Math.random().toString(16).substr(1))
  await mkdir(localFldr)

  const ins = [args.format, ...args.formats.slice(0, -1)]
  const outs = args.formats.slice()
  const pipes = ins
    .map((v, i) => [v, outs[i]])
    .map(([inp, outp]) => renderers.find(({manifest: {input, output}}) => inp === input && outp === output))

  const outfile = await pipes
    .reduce( async (acc, {handler, manifest: {input, output}}) => {
      process.stdout.write(`rendering ${input} to ${output} ... `)
      const sandbox = path.resolve(localFldr, output)
      await mkdir(sandbox)
      const retval = await handler(acc, sandbox, args[output] || null)
      console.log('done')
      return retval
    }, args.source)

  await cpfile(outfile, destination)
  
  console.log(`final render: ${destination}`)
  if (!args.debug) {
    await rmdir(localFldr)
  }
  return destination
}