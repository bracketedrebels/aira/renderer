const path = require('path')
const readdir = require('fs').readdirSync
const exists = require('fs').existsSync
const validateArguments = require('./helpers/validator')
const handleCommand = require('./helpers/handler')

const folder = path.resolve(__dirname, 'renderers')
const renderers = readdir(folder)
  .map( dn => ({
    manifest: path.resolve(folder, dn, 'manifest.json'),
    handler: path.resolve(folder, dn, 'index.js')
  }) )
  .filter( ({manifest, handler}) => exists(manifest) && exists(handler) )
  .map( ({manifest, handler}) => ({
    manifest: require(manifest),
    handler: require(handler)
  }) )
  .filter(({manifest, handler}) => manifest && handler)

const outputs = renderers
  .map(renderer => renderer.manifest.output)
  .filter( (v, i, list) => list.indexOf(v) === i )

exports.command = 'render <source> <format> [formats..]'
exports.desc = 'Renders source data through output pipes'
exports.handler = handleCommand(renderers)
exports.builder = yargs => yargs
  .positional('source', {
    describe: 'Source file to be rendered. Input data type is defined by the file extension',
    type: 'string',
    normalize: true
  })
  .positional('format', {
    describe: 'Format of source data',
    choices: renderers
      .map(renderer => renderer.manifest.input)
      .filter( (v, i, list) => list.indexOf(v) === i ),
    type: 'string'
  })
  .positional('formats', {
    describe: 'Format pipes to render source through. This pipes must be combinable with help of renderers.',
    choices: outputs,
    default: [],
    array: true,
    nargs: 1
  })
  .option('out', {
    alias: 'o',
    describe: 'Where to put last pipe output',
    type: 'string'
  })
  .option('debug', {
    alias: 'd',
    describe: 'Keep temporal folders',
    type: 'boolean',
    default: false
  })
  .options(outputs
    .reduce((acc, fmt) => ({
      ...acc,
      [fmt]: {
        describe: `payload for ${fmt} output format renderer`,
        type: 'string',
        normalize: true
      }
    }), {}))
  .check(validateArguments(renderers))
